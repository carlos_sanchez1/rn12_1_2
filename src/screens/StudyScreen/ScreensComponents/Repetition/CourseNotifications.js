/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState, useContext} from 'react';
import {View, Text, TextInput, FlatList, Alert, Switch} from 'react-native';

import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

import {ObjectId} from 'bson';

import {getRealm} from '../../../../services/realm';

import AddButton from '../../../../components/AddButton';
import BottomModal from '../../../../components/BottomModal';
import SubmitButtons from '../../../../components/BottomModal/submitButtons';
import TextModal from '../../../../components/BottomModal/textModal';
import Button from '../../../../components/Button';
import SwitchSelector from '../../../../components/SwitchSelector';
import DateTimePickerModal from '../../../../components/DateTimePicker';
// import Modal from '../../../../components/Modal';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';

import LinearGradient from 'react-native-linear-gradient';

import {useTheme} from '@react-navigation/native';

import {
  courseColors,
  notificationsRepetition,
  responsive,
  showAlert,
  handleReadableDate,
} from '../../../../utils';

import RealmContext from '../../../../contexts/RealmContext';

import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {
  handleScheduleLocalNotification,
  showNotification,
} from '../../../../notification';

import ReactNativeAN from 'react-native-alarm-notification';

const size = responsive();

const CourseNotifications = ({route, navigation}) => {
  const {colors} = useTheme();

  const {realmApp, setRealmApp, realm, setRealm} = useContext(RealmContext);

  const [courseNotificationsArr, setCourseNotificationsArr] = useState([]);

  const {courseTitle, color, courseId} = route.params;
  console.log('id course', courseId);

  useEffect(() => {
    navigation.setOptions({
      title: `${courseTitle}`,
    });

    realm.write(() => {
      const coursefound = realm.objectForPrimaryKey(
        'Course',
        ObjectId(courseId),
      );

      setCourseNotificationsArr(coursefound.notificationsStudy)
    });
  }, [navigation, courseTitle, courseNotifications]);

  //helper para hacer funcional el arrtimestructure, manejandolo con las posiciones de un arr 0, 1, 2, .....
  const [currentDateTimePickerPosition, setCurrentDateTimePickerPosition] =
    useState(0);

  const [deletedNotification, setDeletedNotification] = useState(false);

  const [editNotification, setEditNotification] = useState(false);

  const [notificationActive, setNotificationActive] = useState(true);
  const [randomStartTime, setRandomStartTime] = useState('');
  const [randomFinishTime, setRandomFinishTime] = useState('');

  const [notificationId, setNotificationId] = useState('');
  const [notificationStudyTitle, setNotificationStudyTitle] = useState('');
  const [notificationStudyBody, setNotificationStudyBody] = useState('');
  const [notificationRepetitionInt, setNotificationRepetitionInt] = useState(1);
  const [privateNotification, setPrivateNotification] = useState(false);

  const [notificationRepetitionsTimeArr, setNotificationRepetitionsTimeArr] =
    useState([]);

  const [
    Helper_in_Edit_notificationRepetitionsTimeArr,
    setHelper_in_Edit_NotificationRepetitionsTimeArr,
  ] = useState([]);

  const [courseNotifications, setCourseNotifications] = useState(false);

  const createNotificationrefBottomModal = useRef();
  const editNotificationrefBottomModal = useRef();
  const deleteOrEditNotificationBottomModal = useRef();

  const notificationTimeStructure = dateTime => {
    class repetitionTimeStructure {
      constructor(id, date) {
        this.id = id;
        this.date = date;
      }
    }

    console.log('hour ISO', dateTime);

    console.log('year', dateTime.getFullYear());
    console.log('hour', dateTime.getHours());
    console.log('minute', dateTime.getMinutes());

    console.log('INTTTT', notificationRepetitionInt);

    console.log('curretn dtp pos', currentDateTimePickerPosition);

    console.log('VALOR DE EDITNOTIFICATION', editNotification);

    if (editNotification) {
      let temporal_helper_arr =
        Helper_in_Edit_notificationRepetitionsTimeArr.map(item => item);

      console.log('TEMPORAL', temporal_helper_arr);

      temporal_helper_arr.push(new repetitionTimeStructure(uuidv4(), dateTime));

      console.log('TEMPORAL pusheadp', temporal_helper_arr);

      setNotificationRepetitionsTimeArr([
        ...notificationRepetitionsTimeArr,
        new repetitionTimeStructure(uuidv4(), dateTime),
      ]);
    } else {
      setNotificationRepetitionsTimeArr(
        notificationRepetitionsTimeArr.concat(
          new repetitionTimeStructure(uuidv4(), dateTime),
        ),
      );
    }
  };

  // console.log('arr lenfht', notificationRepetitionsTimeArr.length)
  // console.log('rep int', notificationRepetitionInt)

  // console.log('ultima pos', notificationRepetitionsTimeArr[currentDateTimePickerPosition])

  if (!deletedNotification) {
    // LAS REPS EN UN DIA SON MENORES QUE EL ARR QUE CONTIENE EL HORARIO ? ENTONCES QUE VAS A HACER
    if (notificationRepetitionsTimeArr.length > notificationRepetitionInt) {
      // VER EN QUE ULTIMA POS DTP ESTABA, DARLE A ESA POS EN VALOR DEL ULTIMO ITEM DEL ARR Y LUEGO BORRAR EL ULTIMO ITEM
      notificationRepetitionsTimeArr[currentDateTimePickerPosition] =
        notificationRepetitionsTimeArr[
          notificationRepetitionsTimeArr.length - 1
        ];

      console.log('time arr mayor que int');

      console.log(
        'el ultimo itemm del arr',
        notificationRepetitionsTimeArr[
          notificationRepetitionsTimeArr.length - 1
        ].id,
      );

      let helper_array_removed_last_item = [];

      helper_array_removed_last_item = notificationRepetitionsTimeArr;

      console.log(
        'filter index',
        notificationRepetitionsTimeArr.map((item, index) => item),
      );

      helper_array_removed_last_item.splice(-1, 1);

      console.log('EL HELPER_REMOVED', helper_array_removed_last_item);

      setNotificationRepetitionsTimeArr(helper_array_removed_last_item);
    }
  } else {
    console.log('deleted');
  }

  console.log('el Arr', notificationRepetitionsTimeArr);

  const handleCreateAndSeveNewNotification = async function (
    notificationtitle,
    body,
    switchValue,
    repeating,
  ) {
    // const realm = await getRealm();

    // notificationTimeStructure(notihr, notimn);

    try {
      realm.write(() => {
        const courseToAddNotification = realm.objectForPrimaryKey(
          'Course',
          ObjectId(courseId),
        );
        courseToAddNotification.notificationsStudy.push({
          id: uuidv4(),
          title: notificationtitle,
          body: body,
          active: switchValue,
          repeat: repeating,
          repetition_time: notificationRepetitionsTimeArr,
        });
        // notificationRepetitionsTimeArr.map((item) =>
        //   courseToAddNotification.notificationsStudy.repetition_time.push(item),
        // );
        // const fireDate = ReactNativeAN.parseDate(new Date(Date.now() + 15000));
        const currentDate = new Date(Date.now());
        const fireDate = `${currentDate.getDate()}-${
          currentDate.getMonth() + 1
        }-${currentDate.getFullYear()} ${notificationRepetitionsTimeArr[0].date.getHours()}:${notificationRepetitionsTimeArr[0].date.getMinutes()}:00`;
        // const fireDate = `18-10-2021 12:29:00`;
        const alarmNotifData = {
          title: notificationtitle,
          message: body,
          channel: 'my_channel_id',
          small_icon: 'ic_launcher',

          // You can add any additional data that is important for the notification
          // It will be added to the PendingIntent along with the rest of the bundle.
          // e.g.
          data: {foo: 'bar'},
        };

        async function saveNotification(params) {
          const alarm = await ReactNativeAN.scheduleAlarm({
            ...alarmNotifData,
            fire_date: fireDate,
          });
          console.log(
            'todas las NOTISS:',
            await ReactNativeAN.getScheduledAlarms(),
          );
          console.log(alarm); // { id: 1 }
        }
        saveNotification();
      });
      setCourseNotifications(!courseNotifications);
      createNotificationrefBottomModal.current.close();
    } catch (error) {
      console.log('ERR', error);
    }
  };

  const handleUpdateAndSaveNotification = async function (
    notificationtitle,
    body,
    switchValue,
    repeating,
    arr,
  ) {
    const foundNotification = courseNotificationsArr.find(
      item => item.id === notificationId,
    );

    let helper_array_copy = notificationRepetitionsTimeArr.map(item => item);

    try {
      realm.write(() => {
        console.log(
          'EL ARR AL EDITAR',
          notificationRepetitionsTimeArr.map(item => item),
        );

        console.log('el helper_array_copy', helper_array_copy);

        foundNotification.title = notificationtitle;
        foundNotification.body = body;
        foundNotification.active = switchValue;
        foundNotification.repeat = repeating;
        // foundNotification.repetition_time = notificationRepetitionsTimeArr.map((item) => item)
      });
      setCourseNotifications(!courseNotifications);
      editNotificationrefBottomModal.current.close();
    } catch (error) {
      console.log('ERR', error);
    }
  };

  const handleDeleteNotification = async notiId => {
    setDeletedNotification(true);
    try {
      realm.write(() => {
        const coursefound = realm.objectForPrimaryKey(
          'Course',
          ObjectId(courseId),
        );
        console.log(
          'removed',
          coursefound.notificationsStudy.filter(item => item.id !== notiId),
        );

        let removedNotification = [];
        removedNotification = coursefound.notificationsStudy.filter(
          item => item.id !== notiId,
        );

        // coursefound.notificationsStudy = [];

        console.log('REMOVEDNotification', removedNotification);

        let removedNotification_helper = [];

        removedNotification.map(item =>
          removedNotification_helper.push({
            id: item.id,
            title: item.title,
            body: item.body,
            active: item.active,
            repeat: item.repeat,
            repetition_time: item.repetition_time.map(item => ({
              id: item.id,
              date: item.date,
            })),
          }),
        );

        console.log('removedNotification_helper', removedNotification_helper);

        coursefound.notificationsStudy = removedNotification_helper;

        // arrtest.push(coursefound.notificationsStudy.filter((item) => item.id !== notiId))
        // console.log('arrtest', arrtest.map((item) => item))
      });
    } catch (error) {
      console.log('ERR', error);
    }

    setCourseNotifications(!courseNotifications);

    deleteOrEditNotificationBottomModal.current.close();
  };

  const handleOnOffNotification = async (notiId, activeValue) => {
    const foundNotification = courseNotificationsArr.find(
      item => item.id === notiId,
    );

    try {
      realm.write(() => {
        foundNotification.active = !activeValue;
      });
    } catch (error) {
      console.log('ERR', error);
    }
    setCourseNotifications(!courseNotifications);
  };

  const notificationsTimeuiStructure = () => {
    let paddingHorizontalContainer;

    let inputHeight;
    let placeHolderFontSize;

    let paddingVerticalButtton;
    let paddingHorizontalButtton;
    let fontSizeButton;

    let switchSelectorFontSize;

    if (size === 'small') {
      paddingHorizontalContainer = 45;

      inputHeight = 25;
      placeHolderFontSize = 10;

      paddingVerticalButtton = 7;
      paddingHorizontalButtton = 18;
      fontSizeButton = 9;

      switchSelectorFontSize = 9;
    } else if (size === 'medium') {
      paddingHorizontalContainer = 40;

      inputHeight = 30;
      placeHolderFontSize = 12;

      paddingVerticalButtton = 8;
      paddingHorizontalButtton = 18;
      fontSizeButton = 13;

      switchSelectorFontSize = 10;
    } else {
      paddingHorizontalContainer = 35;
      inputHeight = 35;
      placeHolderFontSize = 14;

      paddingVerticalButtton = 10;
      paddingHorizontalButtton = 18;
      fontSizeButton = 14;

      switchSelectorFontSize = 12;
    }
    if (notificationRepetitionInt === 1) {
      return (
        <View
          style={{
            // backgroundColor: 'green',
            flexDirection: 'row',
            // justifyContent: 'space-around',
            alignItems: 'center',
            width: '100%',
          }}>
          <View
            style={{
              width: '3%',
            }}>
            <Text style={{color: colors.text}}>1:</Text>
          </View>
          <View
            style={{
              width: '97%',
            }}>
            <DateTimePickerModal
              pressed={value =>
                value
                  ? setCurrentDateTimePickerPosition(0)
                  : setCurrentDateTimePickerPosition(null)
              }
              passAll={true}
              passHourAndMinutes={time => notificationTimeStructure(time)}
              isEditModal={editNotification}
              hour={
                editNotification
                  ? notificationRepetitionsTimeArr[0].date.getHours()
                  : null
              }
              minute={
                editNotification
                  ? notificationRepetitionsTimeArr[0].date.getMinutes()
                  : null
              }
              buttonStyle={{
                paddingVertical: paddingVerticalButtton,
                paddingHorizontal: paddingHorizontalButtton,
                backgroundColor: colors.forms,
                borderRadius: 8,
                width: '100%',
              }}
              fontSizeButton={fontSizeButton}
            />
          </View>
        </View>
      );
    } else if (notificationRepetitionInt === 2) {
      return (
        <View
          style={{
            // backgroundColor: 'red',
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <View
            style={{
              // backgroundColor: 'green',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Text style={{color: colors.text}}>1:</Text>
            <DateTimePickerModal
              pressed={value =>
                value
                  ? setCurrentDateTimePickerPosition(0)
                  : setCurrentDateTimePickerPosition(null)
              }
              passAll={true}
              passHourAndMinutes={time => notificationTimeStructure(time)}
              isEditModal={editNotification}
              hour={
                editNotification
                  ? notificationRepetitionsTimeArr[0].date.getHours()
                  : null
              }
              minute={
                editNotification
                  ? notificationRepetitionsTimeArr[0].date.getMinutes()
                  : null
              }
              buttonStyle={{
                paddingVertical: paddingVerticalButtton,
                paddingHorizontal: paddingHorizontalButtton,
                backgroundColor: colors.forms,
                borderRadius: 8,
              }}
              fontSizeButton={fontSizeButton}
            />
          </View>
          <View
            style={{
              // backgroundColor: 'green',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Text style={{color: colors.text}}>2:</Text>
            <DateTimePickerModal
              pressed={value =>
                value
                  ? setCurrentDateTimePickerPosition(1)
                  : setCurrentDateTimePickerPosition(null)
              }
              passAll={true}
              passHourAndMinutes={time => notificationTimeStructure(time)}
              isEditModal={editNotification}
              hour={
                editNotification
                  ? notificationRepetitionsTimeArr[1].date.getHours()
                  : null
              }
              minute={
                editNotification
                  ? notificationRepetitionsTimeArr[1].date.getMinutes()
                  : null
              }
              buttonStyle={{
                paddingVertical: paddingVerticalButtton,
                paddingHorizontal: paddingHorizontalButtton,
                backgroundColor: colors.forms,
                borderRadius: 8,
              }}
              fontSizeButton={fontSizeButton}
            />
          </View>
        </View>
      );
    } else if (notificationRepetitionInt === 3) {
      return (
        <View
          style={{
            // backgroundColor: 'red',
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <View
            style={{
              // backgroundColor: 'green',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Text style={{color: colors.text}}>1:</Text>
            <DateTimePickerModal
              pressed={value =>
                value
                  ? setCurrentDateTimePickerPosition(0)
                  : setCurrentDateTimePickerPosition(null)
              }
              passAll={true}
              passHourAndMinutes={time => {
                notificationTimeStructure(time);
                console.log('PASS ALL', time);
              }}
              isEditModal={editNotification}
              hour={editNotification ? '0' : null}
              minute={editNotification ? '0' : null}
              buttonStyle={{
                paddingVertical: paddingVerticalButtton,
                paddingHorizontal: paddingHorizontalButtton,
                backgroundColor: colors.forms,
                borderRadius: 8,
              }}
              fontSizeButton={fontSizeButton}
            />
          </View>
          <View
            style={{
              // backgroundColor: 'green',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Text style={{color: colors.text}}>2:</Text>
            <DateTimePickerModal
              pressed={value =>
                value
                  ? setCurrentDateTimePickerPosition(1)
                  : setCurrentDateTimePickerPosition(null)
              }
              passAll={true}
              passHourAndMinutes={time => notificationTimeStructure(time)}
              isEditModal={editNotification}
              hour={editNotification ? '00' : null}
              minute={editNotification ? '00' : null}
              buttonStyle={{
                paddingVertical: paddingVerticalButtton,
                paddingHorizontal: paddingHorizontalButtton,
                backgroundColor: colors.forms,
                borderRadius: 8,
              }}
              fontSizeButton={fontSizeButton}
            />
          </View>
          <View
            style={{
              // backgroundColor: 'green',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Text style={{color: colors.text}}>3:</Text>
            <DateTimePickerModal
              pressed={value =>
                value
                  ? setCurrentDateTimePickerPosition(2)
                  : setCurrentDateTimePickerPosition(null)
              }
              passAll={true}
              passHourAndMinutes={time => notificationTimeStructure(time)}
              isEditModal={editNotification}
              hour={editNotification ? '000' : null}
              minute={editNotification ? '000' : null}
              buttonStyle={{
                paddingVertical: paddingVerticalButtton,
                paddingHorizontal: paddingHorizontalButtton,
                backgroundColor: colors.forms,
                borderRadius: 8,
              }}
              fontSizeButton={fontSizeButton}
            />
          </View>
        </View>
      );
    }
  };

  const createNotificationModal = () => {
    return (
      <BottomModal
        openModal={createNotificationrefBottomModal}
        keyBoardPushContent={false}
        wrapperColor={colors.modalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={670}
        borderRadiusTop={40}
        closeDragDown={true}
        customPaddingHorizontal={true}
        content={
          <View
            style={{
              // paddingHorizontal: 20,
              backgroundColor: null,
              height: '94%',
              justifyContent: 'space-between',
            }}>
            <View>
              <TextModal
                text="Create new repeating notification"
                textTitle={true}
              />
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[
                  courseColors[color].color1,
                  courseColors[color].color2,
                ]}
                style={{
                  marginVertical: 10,
                  backgroundColor: null,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 50,
                  paddingVertical: 5,
                  paddingHorizontal: 14,
                }}>
                <Text style={{color: 'white', fontSize: 13}}>
                  {courseTitle}
                </Text>
              </LinearGradient>
              <TextModal text="Title or Question" textTitle={false} />
              <TextInput
                value={notificationStudyTitle}
                onChangeText={value => setNotificationStudyTitle(value)}
                placeholder="Ej. ¿Cuando fue x Acontecimiento?, significado de x palabra, estudia lo que quieras"
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingVertical: 20,
                  borderRadius: 15,
                  marginBottom: 5,
                }}
              />
              <TextModal
                text="Answer, information or meaning"
                textTitle={false}
              />
              <TextInput
                value={notificationStudyBody}
                onChangeText={value => setNotificationStudyBody(value)}
                placeholder="Ej. Respuesta o significado de tu titlulo"
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingTop: 20,
                  paddingBottom: 50,
                  borderRadius: 20,
                  marginBottom: 5,
                }}
              />
              <TextModal text="Repetitions in the day" textTitle={false} />
              <Text
                style={{fontSize: 10, textAlign: 'center', color: '#8D8D8D'}}>
                number of notifications that will reach you in one day
              </Text>
              <SwitchSelector
                passOptions={notificationsRepetition}
                passValueSelected={value => setNotificationRepetitionInt(value)}
                passFontSize={12}
                passHeight={40}
                // isEditModal={editNotification}
                currentValue={1}
                backgroundColor={colors.forms}
                textColor={colors.text}
                passCustomBtnColor={colors.primary}
                passSelectedTxtColor={colors.forms}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  // width: '63%',
                  justifyContent: 'space-between',
                  marginTop: 17,
                }}>
                <Text>Random Notification Time</Text>
                <Switch
                  value={privateNotification}
                  onValueChange={() =>
                    setPrivateNotification(!privateNotification)
                  }
                />
              </View>
              <TextModal
                text={privateNotification ? 'Random Time' : 'Notifictions time'}
                textTitle={false}
              />

              {privateNotification ? (
                <>
                  <Text
                    style={{
                      fontSize: 10,
                      textAlign: 'center',
                      color: '#8D8D8D',
                    }}>
                    select the time range to receive notifications in random
                    time
                  </Text>
                  <View
                    style={{
                      // backgroundColor: 'red',
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View
                      style={{
                        // backgroundColor: 'green',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                      }}>
                      <Ionicons
                        name="md-sunny"
                        color={colors.text}
                        size={18}
                        style={{
                          marginRight: 2,
                        }}
                      />
                      <DateTimePickerModal
                        pressed={value => {}}
                        passAll={true}
                        passHourAndMinutes={time => setRandomStartTime(time)}
                        isEditModal={editNotification}
                        hour={
                          editNotification ? randomStartTime.getHours() : null
                        }
                        minute={
                          editNotification
                            ? randomStartTime[0].date.getMinutes()
                            : null
                        }
                        buttonStyle={{
                          paddingVertical: 10,
                          paddingHorizontal: 15,
                          backgroundColor: colors.forms,
                          borderRadius: 8,
                        }}
                        fontSizeButton={12}
                      />
                    </View>
                    <View
                      style={{
                        // backgroundColor: 'green',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                      }}>
                      <Entypo
                        name="moon"
                        color={colors.text}
                        size={18}
                        style={{
                          marginRight: 2,
                        }}
                      />
                      <DateTimePickerModal
                        pressed={value => {}}
                        passAll={true}
                        passHourAndMinutes={time => setRandomFinishTime(time)}
                        isEditModal={editNotification}
                        hour={
                          editNotification ? randomFinishTime.getHours() : null
                        }
                        minute={
                          editNotification
                            ? randomFinishTime.getMinutes()
                            : null
                        }
                        buttonStyle={{
                          paddingVertical: 10,
                          paddingHorizontal: 15,
                          backgroundColor: colors.forms,
                          borderRadius: 8,
                        }}
                        fontSizeButton={12}
                      />
                    </View>
                  </View>
                </>
              ) : (
                notificationsTimeuiStructure()
              )}
            </View>
            <SubmitButtons
              leftButtonFunction={() =>
                createNotificationrefBottomModal.current.close()
              }
              leftButtonText="Cancel"
              rightButtonFunction={() => {
                notificationStudyTitle.length === 0 &&
                notificationStudyBody.length === 0
                  ? Alert.alert('Rellena los campos')
                  : handleCreateAndSeveNewNotification(
                      notificationStudyTitle,
                      notificationStudyBody,
                      notificationActive,
                      notificationRepetitionInt,
                    );
              }}
              rightButtonText="Create"
            />
            {/* <View
              style={{
                backgroundColor: null,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              }}>
              <Button
                onPress={() => createNotificationrefBottomModal.current.close()}
                content={
                  <View
                    style={{
                      borderColor: '#3F3F3F',
                      borderWidth: 1,
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: '#3F3F3F'}}>Cancelar</Text>
                  </View>
                }
              />
              <Button
                onPress={() =>
                  notificationStudyTitle.length === 0 &&
                  notificationStudyBody.length === 0
                    ? Alert.alert('Rellena los campos')
                    : handleCreateAndSeveNewNotification(
                        notificationStudyTitle,
                        notificationStudyBody,
                        notificationActive,
                        notificationRepetitionInt,
                      )
                }
                content={
                  <View
                    style={{
                      backgroundColor: '#0B6DF6',
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: 'white'}}>Crear</Text>
                  </View>
                }
              />
            </View> */}
          </View>
        }
      />
    );
  };

  const editNotificationModal = () => {
    return (
      <BottomModal
        openModal={editNotificationrefBottomModal}
        keyBoardPushContent={false}
        wrapperColor={colors.modalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={660}
        borderRadiusTop={40}
        closeDragDown={true}
        content={
          <View
            style={{
              paddingHorizontal: 20,
              backgroundColor: null,
              height: '94%',
              justifyContent: 'space-between',
            }}>
            <View>
              <TextModal text="Edit repeating notification" textTitle={true} />
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[
                  courseColors[color].color1,
                  courseColors[color].color2,
                ]}
                style={{
                  marginVertical: 10,
                  backgroundColor: null,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 50,
                  paddingVertical: 5,
                  paddingHorizontal: 14,
                }}>
                <Text style={{color: 'white', fontSize: 13}}>
                  {courseTitle}
                </Text>
              </LinearGradient>
              <TextModal text="Title or Question" textTitle={false} />
              <TextInput
                //   autoFocus
                value={notificationStudyTitle}
                onChangeText={value => setNotificationStudyTitle(value)}
                placeholder="Ej. ¿Cuando fue x Acontecimiento?, significado de x palabra, estudia lo que quieras"
                //   enablesReturnKeyAutomatically
                //   onSubmitEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                //   onEndEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingVertical: 20,
                  borderRadius: 15,
                  marginBottom: 5,
                }}
              />
              <TextModal
                text="Answer, information or meaning"
                textTitle={false}
              />
              <TextInput
                //   autoFocus
                value={notificationStudyBody}
                onChangeText={value => setNotificationStudyBody(value)}
                placeholder="Ej. Respuesta o significado de tu titlulo"
                //   enablesReturnKeyAutomatically
                //   onSubmitEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                // onEndEditing={() => addCourserefBottomModal.current.close()}
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingTop: 20,
                  paddingBottom: 50,
                  borderRadius: 20,
                  marginBottom: 5,
                }}
              />
              {editNotification ? null : (
                <>
                  <TextModal text="Repetitions in the day" textTitle={false} />
                  <Text
                    style={{
                      fontSize: 10,
                      textAlign: 'center',
                      color: '#8D8D8D',
                    }}>
                    number of notifications that will reach you in one day
                  </Text>
                  <SwitchSelector
                    passOptions={notificationsRepetition}
                    passValueSelected={value =>
                      setNotificationRepetitionInt(value)
                    }
                    passFontSize={12}
                    passHeight={40}
                    isEditModal={editNotification}
                    currentValue={notificationRepetitionInt - 1}
                    backgroundColor={colors.forms}
                    textColor={colors.text}
                    passCustomBtnColor={colors.primary}
                    passSelectedTxtColor={colors.forms}
                  />
                  <TextModal text="Notifictions time" textTitle={false} />
                  {notificationsTimeuiStructure()}
                </>
              )}
            </View>
            <View
              style={{
                backgroundColor: null,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              }}>
              <Button
                onPress={() => editNotificationrefBottomModal.current.close()}
                content={
                  <View
                    style={{
                      borderColor: '#3F3F3F',
                      borderWidth: 1,
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: '#3F3F3F'}}>Cancelar</Text>
                  </View>
                }
              />
              <Button
                onPress={() =>
                  notificationStudyTitle.length === 0 &&
                  notificationStudyBody.length === 0
                    ? Alert.alert('Nada que actualizar')
                    : handleUpdateAndSaveNotification(
                        notificationStudyTitle,
                        notificationStudyBody,
                        notificationActive,
                        notificationRepetitionInt,
                        notificationRepetitionsTimeArr,
                      )
                }
                content={
                  <View
                    style={{
                      backgroundColor: '#0B3FF6',
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: 'white'}}>Editar</Text>
                  </View>
                }
              />
            </View>
          </View>
        }
      />
    );
  };

  const deleteOrEditRoutineModal = () => {
    let paddingVerticalContainer;
    let paddingHorizontalPlusIconContainer;
    let icons;
    let fontSize;
    if (size === 'small') {
      paddingVerticalContainer = 15;
      paddingHorizontalPlusIconContainer = 20;
      icons = 35;
      fontSize = 10;
    } else if (size === 'medium') {
      paddingVerticalContainer = 22;
      paddingHorizontalPlusIconContainer = 28;
      icons = 45;
      fontSize = 12;
    } else {
      //large screen
      paddingVerticalContainer = 25;
      paddingHorizontalPlusIconContainer = 30;
      icons = 48;
      fontSize = 15;
    }
    return (
      <BottomModal
        openModal={deleteOrEditNotificationBottomModal}
        wrapperColor={colors.modalWrapper}
        muchContent={false}
        borderRadiusTop={40}
        closeDragDown={true}
        content={
          <View
            style={
              {
                // backgroundColor: 'yellow',
              }
            }>
            <Button
              onPress={() => {
                setEditNotification(false);
                showAlert(
                  'Eiminar Notificacion',
                  '¿Deseas Eliminar permanentemente la notificacion?',
                  () => {
                    console.log('cancelado');
                  },
                  () => {
                    console.log('eliminado');
                    handleDeleteNotification(notificationId);
                  },
                );
              }}
              content={
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    // backgroundColor: 'pink',
                  }}>
                  <FontAwesome
                    name="trash"
                    color={colors.text}
                    size={35}
                    style={{marginRight: 20}}
                  />
                  <Text style={{fontSize: 16, color: colors.text}}>
                    Delete Notification
                  </Text>
                </View>
              }
              styleBtn={{
                paddingHorizontal: 25,
                paddingVertical: 15,
                // backgroundColor: 'orange',
              }}
            />
            <Button
              onPress={() => {
                setEditNotification(true);
                editNotificationrefBottomModal.current.open();
              }}
              content={
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    // backgroundColor: 'pink',
                  }}>
                  <FontAwesome
                    name="edit"
                    color={colors.text}
                    size={35}
                    style={{marginRight: 15}}
                  />
                  <Text style={{fontSize: 16, color: colors.text}}>
                    Edit Notification
                  </Text>
                  {/* {createNotificationModal()} */}
                </View>
              }
              styleBtn={{
                paddingHorizontal: 25,
                paddingVertical: 15,
                // backgroundColor: 'orange',
              }}
            />
            {editNotificationModal()}
          </View>
        }
      />
    );
  };

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        backgroundColor: null,
      }}>
      {courseNotificationsArr.length > 0 ? (
        <View style={{width: '100%', height: '100%'}}>
          <FlatList
            data={courseNotificationsArr}
            keyExtractor={item => item.id}
            style={{backgroundColor: null, padding: 13}}
            numColumns={1}
            renderItem={({item}) => (
              <View
                style={{
                  backgroundColor: colors.forms,
                  alignItems: 'center',
                  flexDirection: 'column',
                  width: '100%',
                  padding: 20,
                  marginVertical: 15,
                  borderRadius: 20,
                }}>
                <View
                  style={{
                    // backgroundColor: 'red',
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                  }}>
                  <LinearGradient
                    start={{x: 0.0, y: 0.25}}
                    end={{x: 0.5, y: 1.0}}
                    colors={
                      item.active
                        ? [
                            courseColors[color].color1,
                            courseColors[color].color2,
                          ]
                        : [
                            colors.linearNotificationBoxDesactivate,
                            colors.linearNotificationBoxDesactivate,
                          ]
                    }
                    style={{
                      backgroundColor: null,
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 50,
                      paddingVertical: 6,
                      paddingHorizontal: 14,
                    }}>
                    <Text
                      style={{
                        color: item.active
                          ? 'white'
                          : colors.textNotificationLinearBoxDesactivate,
                        fontSize: 13,
                      }}>
                      {courseTitle}
                    </Text>
                  </LinearGradient>
                  <Button
                    onPress={() => {
                      setNotificationId(item.id);
                      setNotificationStudyTitle(item.title);
                      setNotificationStudyBody(item.body);
                      setNotificationRepetitionInt(item.repeat);
                      setNotificationRepetitionsTimeArr(item.repetition_time);
                      setHelper_in_Edit_NotificationRepetitionsTimeArr(
                        item.repetition_time,
                      );
                      deleteOrEditNotificationBottomModal.current.open();
                    }}
                    content={
                      <SimpleLineIcons
                        name="options"
                        color={colors.text}
                        size={25}
                      />
                    }
                  />
                </View>
                <View
                  style={{
                    // backgroundColor: 'orange',
                    width: '100%',
                  }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      color: item.active
                        ? colors.text
                        : colors.textNotificationDesactivate,
                      marginTop: 20,
                      marginBottom: 10,
                      fontSize: 18,
                    }}>
                    {item.title}
                  </Text>
                  <View
                    style={{
                      // backgroundColor: 'blue',
                      paddingVertical: 10,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      // width: '100%',
                    }}>
                    <View
                      style={{
                        // backgroundColor: 'pink',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        width: '15%',
                      }}>
                      <FontAwesome
                        name="repeat"
                        color={
                          item.active
                            ? colors.text
                            : colors.textNotificationDesactivate
                        }
                        size={22}
                      />
                      <Text
                        style={{
                          color: item.active
                            ? colors.text
                            : colors.textNotificationDesactivate,
                          fontSize: 15,
                          marginLeft: 2,
                        }}>
                        {item.repeat}
                      </Text>
                    </View>
                    <View
                      style={{
                        // backgroundColor: 'yellow',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-evenly',
                        width: '85%',
                      }}>
                      {item.repetition_time.map(item2 => (
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <MaterialCommunityIcons
                            name="bell-ring"
                            color={
                              item.active
                                ? colors.text
                                : colors.textNotificationDesactivate
                            }
                            size={18}
                            style={{
                              marginRight: 2,
                            }}
                          />
                          <Text
                            style={{
                              color: item.active
                                ? colors.text
                                : colors.textNotificationDesactivate,
                            }}>
                            {handleReadableDate(
                              item2.date?.getHours(),
                              item2.date?.getMinutes(),
                            )}
                          </Text>
                        </View>
                      ))}
                    </View>
                  </View>
                  <Switch
                    value={item.active}
                    style={{marginTop: 10}}
                    onValueChange={
                      () => handleOnOffNotification(item.id, item.active)
                      // setNotificationActive(!notificationActive)
                    }
                  />
                </View>
              </View>
            )}
          />
          <View
            style={{
              position: 'absolute',
              left: '80%',
              top: '90%',
              backgroundColor: 'green',
            }}>
            <AddButton
              onPress={() => {
                setDeletedNotification(false);
                setEditNotification(false);
                setNotificationId('');
                setNotificationStudyTitle('');
                setNotificationStudyBody('');
                setNotificationRepetitionInt(1);
                setNotificationRepetitionsTimeArr([]);
                setPrivateNotification(false);
                setRandomStartTime('');
                setRandomFinishTime('');
                createNotificationrefBottomModal.current.open();
              }}
              iconSize={60}
            />
          </View>
        </View>
      ) : (
        <View
          style={{
            backgroundColor: 'red',
            alignItems: 'center',
          }}>
          <Text>No tienes notificacines crea una</Text>
          <Text style={{marginBottom: 20, marginTop: 8}}>crear</Text>
          <AddButton
            onPress={() => {
              setDeletedNotification(false);
              setEditNotification(false);
              setNotificationId('');
              setNotificationStudyTitle('');
              setNotificationStudyBody('');
              setNotificationRepetitionInt(1);
              setNotificationRepetitionsTimeArr([]);
              setPrivateNotification(false);
              setRandomStartTime('');
              setRandomFinishTime('');
              createNotificationrefBottomModal.current.open();
            }}
            iconSize={55}
          />
        </View>
      )}
      {createNotificationModal()}
      {deleteOrEditRoutineModal()}
    </View>
  );
};

export default CourseNotifications;
